# CHANGELOG for truestory

This file is used to list changes made in each version of truestory.

## 0.1.0:

* Initial release of truestory

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
