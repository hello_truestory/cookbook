#
# Cookbook Name:: truestory
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'nginx'

service 'nginx' do 
	supports [:status]
	action[:start]
end
